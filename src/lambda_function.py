import json
import requests
import boto3

from logger import Logger

ONBOARD_STATE = {
    'not_onboarded': "Not onboarded",
    'failed_to_onboard': "Failed to onboard",
    'onboarded': "Onboarded"
}

ssm = boto3.client('ssm')


def get_ssm_parameter(parameter_name, with_decryption=False):
    ssm_parameter = ssm.get_parameter(
        Name=parameter_name,
        WithDecryption=with_decryption
    )
    return ssm_parameter['Parameter']['Value']


logger = Logger(get_ssm_parameter('FR_LAB_AWX_Onboarding_Debug_Level'))
ec2_region = get_ssm_parameter('FR_LAB_AWX_Onboarding_Region')
subnet_id = get_ssm_parameter('FR_LAB_AWX_Onboarding_Subnet')
onboard_tag = get_ssm_parameter('FR_LAB_AWX_Onboarding_Tag')
name_tag = get_ssm_parameter('FR_LAB_AWX_Onboarding_NameTag')
ip_tag = get_ssm_parameter('FR_LAB_AWX_Onboarding_IpTag')
ec2_resource = boto3.resource('ec2', ec2_region)
ec2_client = boto3.client('ec2', region_name=ec2_region)


def get_ec2_info(instance_id):
    logger.trace(instance_id, caller_name='get_ec2_instance')
    instance = ec2_resource.Instance(instance_id)
    image = ec2_resource.Image(instance.image_id)

    return instance, image


def get_tag(instance, key):
    logger.trace(instance, caller_name='get_tag')
    value = None
    for tags in instance.tags:
        if tags["Key"] == key:
            value = tags["Value"]

    return value


def set_tag(instance, key, value):
    logger.trace(instance, caller_name='set_tag_tag')
    resp_create_tag = ec2_client.create_tags(
        Resources=[
            instance.instance_id,
        ],
        Tags=[
            {
                'Key': key,
                'Value': value
            }
        ]
    )

    return resp_create_tag


def get_onboard_state(instance):
    logger.trace(instance, caller_name='get_onboard_state')
    onboard_state = get_tag(instance, onboard_tag)

    if not onboard_state:
        onboard_state = ONBOARD_STATE['not_onboarded']

    return onboard_state


# Source: https://github.com/cyberark/cyberark-aws-auto-onboarding/blob
# /674908ca4304f9ead5451fec078fbc93189a3910/src/shared_libraries/instance_processing.py#L154
def get_os_distribution_user(image_description):
    logger.trace(image_description, caller_name='get_os_distribution_user')
    if "centos" in image_description.lower():
        linux_username = "centos"
    elif "rocky" in image_description.lower():
        linux_username = "rocky"
    elif "ubuntu" in image_description.lower():
        linux_username = "ubuntu"
    elif "debian" in image_description.lower():
        linux_username = "admin"
    elif "fedora" in image_description.lower():
        linux_username = "fedora"
    elif "opensuse" in image_description.lower():
        linux_username = "root"
    else:
        linux_username = "ec2-user"

    return linux_username


def lambda_handler(event, context):
    logger.trace(event, context, caller_name='lambda_handler')

    logger.debug('Parsing event')
    data = None
    try:
        data = event["requestPayload"]
    except Exception as e:
        logger.error(f"Error on retrieving Message Data from Event Message. Error: {e}")
        exit()

    instance_id = None
    try:
        instance_id = data["detail"]["instance-id"]
        logger.debug(f"Instance ID: {instance_id}")
    except Exception as e:
        logger.error(f"Error on retrieving Instance Id from Event Message. Error: {e}")
        exit()

    action_type = None
    try:
        action_type = data["detail"]["state"]
        logger.debug(f"Instance state: {action_type}")
    except Exception as e:
        logger.error(f"Error on retrieving Action Type from Event Message. Error: {e}")
        exit()

    if action_type not in ['running', 'terminated']:
        return {
            'statusCode': 200,
            'body': json.dumps(f"Instance is {action_type}, no job to trigger")
        }

    ######################################
    # Fetch instance properties from AWS #
    ######################################
    instance, image = get_ec2_info(instance_id)
    name = get_tag(instance, name_tag)
    onboard_state = get_onboard_state(instance)
    instance_default_user = get_os_distribution_user(image.description)

    logger.debug(f"Instance name: {name}")
    logger.debug(f"Instance address: {instance.private_ip_address}")
    logger.debug(f"Instance default user: {instance_default_user}")
    logger.debug(f"Instance onboard status: {onboard_state}")
    logger.debug(f"Image description: {image.description}")

    ###############################
    # Exit when no job to trigger #
    ###############################
    if action_type != 'terminated' and instance.subnet_id != subnet_id:
        # instance.subnet_id is null on terminated instances
        return {
            'statusCode': 200,
            'body': json.dumps('Instance not part of the onboarding subnet')
        }

    if onboard_state == ONBOARD_STATE['onboarded'] and action_type == 'running':
        return {
            'statusCode': 200,
            'body': json.dumps('Instance already onboarded, no job to trigger')
        }

    if onboard_state in [ONBOARD_STATE['failed_to_onboard'],
                         ONBOARD_STATE['not_onboarded']] and action_type == 'terminated':
        return {
            'statusCode': 200,
            'body': json.dumps('Instance never onboarded, no job to trigger')
        }

    ###########################
    # Prepare API call to AWX #
    ###########################
    if action_type == 'terminated':
        awx_job_id = get_ssm_parameter('FR_LAB_AWX_Onboarding_Delete_Job_Id')
        # must load last known ip, instance.private_ip_address is null
        instance_ip = get_tag(instance, ip_tag)

    else:
        awx_job_id = get_ssm_parameter('FR_LAB_AWX_Onboarding_Job_Id')
        instance_ip = instance.private_ip_address

    awx_url = get_ssm_parameter('FR_LAB_AWX_Onboarding_Url')
    awx_full_url = f"{awx_url}/api/v2/job_templates/{awx_job_id}/launch/"
    awx_verify_ssl = get_ssm_parameter('FR_LAB_AWX_Onboarding_Verify_Ssl') != 'False'

    awx_login = get_ssm_parameter('FR_LAB_AWX_Onboarding_Username')
    awx_password = get_ssm_parameter('FR_LAB_AWX_Onboarding_Password', with_decryption=True)

    resp = requests.post(
        awx_full_url,
        auth=requests.auth.HTTPBasicAuth(awx_login, awx_password),
        verify=awx_verify_ssl,
        json={
            "extra_vars": {
                "instance_ip": instance_ip,
                "instance_username": instance_default_user,
                "instance_name": name
            }
        }
    )

    logger.debug(f"AWX response status: {resp.status_code}")
    logger.debug(f"AWX response content: {resp.text}")

    ###############################
    # Send lambda return response #
    ###############################
    if action_type == 'running':
        if resp.status_code != requests.codes.created:
            set_tag(instance, onboard_tag, ONBOARD_STATE['failed_to_onboard'])
            return {
                'statusCode': resp.status_code,
                'body': json.dumps('Failed to trigger AWX onboarding job')
            }
        else:
            set_tag(instance, onboard_tag, ONBOARD_STATE['onboarded'])
            set_tag(instance, ip_tag, instance.private_ip_address)
            return {
                'statusCode': 200,
                'body': json.dumps('AWX onboarding job triggered')
            }
    elif action_type == 'terminated':
        if resp.status_code != requests.codes.created:
            return {
                'statusCode': resp.status_code,
                'body': json.dumps('Failed to trigger AWX delete job')
            }
        else:
            return {
                'statusCode': 200,
                'body': json.dumps('AWX delete job triggered')
            }
