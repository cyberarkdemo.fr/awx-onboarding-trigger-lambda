#!/bin/bash

. /utils

LAMBDA_ZIP=${LAMBDA_ZIP:-awx_onboarding_trigger.zip}

# Fetch python requirements
log_info "Downloading dependencies..."
pip install -r /tmp/requirements.txt --target /tmp/cache/package
log_done "Dependencies installed"

# Create project archive
log_info "Building ${LAMBDA_ZIP}..."
rm -f ${LAMBDA_ZIP}

cd /tmp/cache/package || exit
zip -q -r9 /tmp/output/${LAMBDA_ZIP} .

cd /tmp/src/ || exit
zip -q -g /tmp/output/${LAMBDA_ZIP} *.py

log_done "${LAMBDA_ZIP} built"