#!/bin/bash

BINDIR=$(dirname "$(readlink -f $0)")
. $BINDIR/docker/utils

log_info "Building source builder docker image."
docker build --platform linux/amd64 -t awx_onboarding_trigger_builder $BINDIR/docker
log_done "Finished building docker image"

log_info "Packaging lambdas code in dist/lambdas"
docker run  --platform linux/amd64 \
            --rm \
            --mount type=bind,source="$BINDIR/src",target=/tmp/src \
            --mount type=bind,source="$BINDIR/src/requirements.txt",target=/tmp/requirements.txt \
            --mount type=bind,source="$BINDIR/dist",target=/tmp/output \
            -e LAMBDA_ZIP="awx_onboarding_trigger.zip" \
            awx_onboarding_trigger_builder
log_done "Finished packaging lambdas code"
#            --user "$(id -u):$(id -g)" \
